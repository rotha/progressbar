package progressbar;

import java.util.ArrayList;

/**
 * The Timer class provides a simple timing utility to measure elapsed time,
 * primarily used in conjunction with the Progressbar class.
 */
public class Timer {
    private long currentTime;
    private long timeStart;
    private boolean isRunning;
    private ArrayList<Long> timeStops;

    /**
     * Initializes a new instance of the Timer class.
     */
    public Timer() {
        this.currentTime = 0;
        this.timeStart = 0;
        isRunning = false;
        this.timeStops = new ArrayList<>();
    }

    /**
     * Starts the timer. If the timer is already running, an error message is printed.
     */
    public void startTimer() {
        if (!isRunning) {
            this.timeStart = System.currentTimeMillis();
            isRunning = true;
        } else {
            System.err.println("Timer is already running. You cannot start a new one.");
        }
    }

    /**
     * Stops the timer and records the elapsed time. If the timer is not running,
     * an error message is printed.
     */
    public void stopTimer() {
        stopTimer(true);
    }

    /**
     * Stops the timer, records the elapsed time, and optionally adds the elapsed time
     * to the list of time stops.
     *
     * @param addToStopTime A boolean indicating whether to add the current time
     *                      to the list of time stops.
     */
    public void stopTimer(boolean addToStopTime) {
        if (isRunning) {
            this.currentTime += System.currentTimeMillis() - this.timeStart;
            if (addToStopTime) {
                this.timeStops.add(currentTime);
            }
            isRunning = false;
        } else {
            System.err.println("Timer is not running. You cannot stop it.");
        }
    }

    /**
     * Gets the current elapsed time in milliseconds.
     *
     * @return The current elapsed time in milliseconds.
     */
    public long getCurrentTimeInMilliSeconds() {
        if (isRunning) {
            return (System.currentTimeMillis() - this.timeStart);
        } else {
            return this.currentTime;
        }
    }

    /**
     * Gets the current elapsed time in seconds.
     *
     * @return The current elapsed time in seconds.
     */
    public double getCurrentTimeInSeconds() {
        if (isRunning) {
            return (System.currentTimeMillis() - this.timeStart) / (1000 * 1.0);
        } else {
            return this.currentTime / (1000 * 1.0);
        }
    }

    /**
     * Gets the list of recorded stop times.
     *
     * @return An ArrayList containing the recorded stop times.
     */
    public ArrayList<Long> getTimeStops() {
        return timeStops;
    }
}

