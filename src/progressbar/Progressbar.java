package progressbar;

import java.text.DecimalFormat;
import java.util.concurrent.atomic.AtomicInteger;
/**
 * The Progressbar class provides a thread-safe progress bar implementation for console applications in Java.
 * It displays the progress of a task with a visual bar and percentage completion.
 */
public class Progressbar {
    public final String progressBarString = "#";
    public final String progressBarStartString = "[";
    public final String progressBarEndString = "]";
    public final int barLength = 100;
    DecimalFormat percentageFormat;
    Timer timer;

    String title;
    int numberOfSteps;
    public AtomicInteger currentStepCounter;
    public AtomicInteger currentBarCounter;

    /**
     * Initializes a new instance of the Progressbar with a specified title and number of steps.
     *
     * @param title         A String representing the title of the progress bar.
     * @param numberOfSteps An int representing the total number of steps to complete the task.
     */
    public Progressbar(String title, int numberOfSteps) {
        this.title = title;
        this.numberOfSteps = numberOfSteps;
        currentStepCounter = new AtomicInteger(0);
        this.currentBarCounter = new AtomicInteger(0);
        this.percentageFormat =new DecimalFormat("0.00");
        this.timer=new Timer();
        timer.startTimer();
        System.out.print(getProgressBar());
    }

    /**
     * Increments the progress of the bar by one step. Updates the visual representation of the progress bar in the console.
     * Automatically stops the timer and prints the final state when the task is completed.
     */
    public void countUp() {
        int currentStep = currentStepCounter.incrementAndGet();

        double currentFactor = 1.0*numberOfSteps / currentStep;

        int currentBarsShouldBe = (int) (barLength / currentFactor);

        if (currentBarsShouldBe != currentBarCounter.get()) {
            currentBarCounter.set(currentBarsShouldBe);
            System.out.print(getProgressBar());
        }


        if (numberOfSteps==currentStepCounter.get()){
            System.out.println();
            timer.stopTimer();
        }


    }

    private String getProgressBar(){
        StringBuilder sb=new StringBuilder("\r");
        sb.append(this.title);
        sb.append(": ");
        double percentage=this.currentStepCounter.get()*1.0/this.numberOfSteps;
        sb.append(percentageFormat.format(percentage*100)).append("% ");
        sb.append(progressBarStartString);
        sb.append(progressBarString.repeat(Math.max(0, currentBarCounter.get() + 1)));
        sb.append(" ".repeat(Math.max(0, barLength - (currentBarCounter.get() + 1))));
        sb.append(progressBarEndString).append(" time:");
        sb.append((timer.getCurrentTimeInSeconds()));

        return sb.toString();
    }

}



