# Java Progressbar Project

## Overview

The Java Progressbar Project provides a thread-safe progress bar implementation for console applications. The project includes a `Progressbar` class for displaying progress and a `Timer` class for measuring elapsed time.

## Progressbar Class

The `Progressbar` class features:
- Customizable title and number of steps for the progress bar.
- Thread-safe implementation.
- Real-time updates of the progress bar during task execution.
- Automatic stopping of the timer and display of the final state upon completion.

## Timer Class

The `Timer` class offers:
- Simple timing utility to measure elapsed time.
- Start and stop functionality for accurate time measurement.
- Option to record stop times in a list.

## Usage

1. **Initialization:** Create an instance of the `Progressbar` class with a title and the total number of steps in your task.

    ```java
    Progressbar progressBar = new Progressbar("Task Progress", totalSteps);
    ```

2. **Increment Progress:** Call the `countUp()` method each time a step in the process is completed.

    ```java
    progressBar.countUp();
    ```

3. **Retrieve Elapsed Time:** If needed, use the `Timer` class to measure and retrieve elapsed time.

    ```java
    Timer timer = new Timer();
    timer.startTimer();
    // Perform your task
    timer.stopTimer();
    System.out.println("Elapsed Time: " + timer.getCurrentTimeInSeconds() + " seconds");
    ```

## Integration

1. **Import Project:** Import the project into your preferred Java development environment (e.g., IntelliJ IDEA).

2. **Add to Project:** Add the `Progressbar` and `Timer` classes to your project.

3. **Use in Code:** Follow the usage guidelines to integrate the progress bar and timer into your Java application.

